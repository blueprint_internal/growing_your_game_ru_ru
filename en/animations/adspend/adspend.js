

function AdSpend(resources)
{
	AdSpend.resources = resources;
}
AdSpend.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(800, 800, Phaser.CANVAS, 'AdSpend', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this });
	},

	preload: function()
	{

        this.game.stage.backgroundColor = '#ffffff';
        this.game.scale.maxWidth = 800;
    	this.game.scale.maxHeight = 800;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('bckg', AdSpend.resources.bckg);
    	this.game.load.image('funnelb', AdSpend.resources.funnelb);
    	this.game.load.image('funnelf', AdSpend.resources.funnelf);
    	this.game.load.image('cracks', AdSpend.resources.cracks);
    	this.game.load.image('arrowb', AdSpend.resources.arrowb);
    	this.game.load.image('arrowl', AdSpend.resources.arrowl);
    	this.game.load.image('arrowd', AdSpend.resources.arrowd);
    	this.game.load.image('arrowd2', AdSpend.resources.arrowd2);
    	this.game.load.image('coinb', AdSpend.resources.coind);
    	this.game.load.image('coind', AdSpend.resources.coind);
    	this.game.load.image('coind2', AdSpend.resources.coinsm);
    	this.game.load.image('titleslab', AdSpend.resources.titleslab);
    	    //should prevent mobile scroll issue
	    this.game.input.touch.preventDefault = false;
	},

	create: function(evt)
	{
    //Call the animation build function
    this.parent.buildAnimation();

	},

	buildAnimation: function()
	{

     //Background
     this.bckg = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'bckg');
     this.bckg.anchor.set(0.5);

	//LeftArrow
	this.arrowl = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY + 80, 'arrowl');

	////////////Right Arrow
	this.arrowb = this.game.add.sprite(this.game.world.centerX,250, 'arrowb');

	///FUNNNEL BACK
    this.funnelb = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY-200,'funnelb');
    this.funnelb.anchor.set(0.5);

    //ARROW	
    this.arrowd = this.game.add.sprite(this.game.world.centerX,-500, 'arrowd');
       this.arrowd2 = this.game.add.sprite(this.game.world.centerX - 50,350, 'arrowd2');

	this.arrowd.x= this.arrowd.x - this.arrowd.width / 2;

	//COINS
    this.coinb = this.game.add.sprite(this.game.world.centerX - 100,-500, 'coinb');//
    this.coind = this.game.add.sprite(this.game.world.centerX + 15,-900, 'coind');//
    
    //FUNNEL

     this.funnelf = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY-15,'funnelf');
    this.funnelf.anchor.set(0.5);


    this.cracks = this.game.add.sprite(this.game.world.centerX+13, this.game.world.centerY-60,'cracks');
    this.cracks.anchor.set(0.5);
    this.cracks.alpha = 0;

    //STYLE
var style = AdSpend.resources.headerStyle;

var styleArrows = AdSpend.resources.arrowStyle;

var styleFunnel = AdSpend.resources.funnelStyle;

var styleFooter = AdSpend.resources.footerStyle;


    this.titleslab = this.game.add.sprite(this.game.world.centerX, 70, 'titleslab');

    this.titleslab.anchor.set(0.5);

		//TEXT ELEMENTS
		this.header_1 = this.game.add.text(this.game.world.centerX, 40, AdSpend.resources.headertext, style);
		this.header_1.anchor.x = Math.round(this.header_1.width * 0.5) / this.header_1.width;

		this.pfield_1 = this.game.add.text(this.game.world.centerX, 230, AdSpend.resources.ico1text, styleFunnel);
		this.pfield_1.anchor.x = Math.round(this.pfield_1.width * 0.5) / this.pfield_1.width;

		this.pfield_2 = this.game.add.text(this.game.world.centerX, 280, AdSpend.resources.ico2text, styleFunnel);
		this.pfield_2.anchor.x = Math.round(this.pfield_2.width * 0.5) / this.pfield_2.width;

		this.pfield_3 = this.game.add.text(this.game.world.centerX, 330, AdSpend.resources.ico3text, styleFunnel);
		this.pfield_3.anchor.x = Math.round(this.pfield_3.width * 0.5) / this.pfield_3.width;

		///LEAKAGE
		var eightWidth = this.game.world.centerX / 4;
		var threeQuarterWidth = eightWidth * 6;
		this.pfield_4l = new Phaser.Text(this.game,0,0,AdSpend.resources.ico4text);
		this.pfield_4l = this.game.add.text(this.game.world.centerX-((this.game.world.width/4)+20), this.game.world.centerY-20, AdSpend.resources.ico4text, styleArrows);
		this.pfield_4l.lineSpacing = -20;
		this.pfield_4l.anchor.set(0.5);
		this.pfield_4l.alpha = 0;
		this.pfield_4l.smoothed=false;
		this.pfield_4 = this.game.add.text(this.game.world.centerX+((this.game.world.width/4)+20), this.game.world.centerY-50, AdSpend.resources.ico4text, styleArrows);
		this.pfield_4.lineSpacing = -20;
		this.pfield_4.smoothed = false;
		this.pfield_4.anchor.set(0.5);
		this.pfield_4.alpha = 0;

		//MAXMIZE VALUE
this.pfield_5 = this.game.add.text(this.game.world.centerX, 665, AdSpend.resources.ico5text, styleFooter);
this.pfield_5.anchor.set(0.5);
this.pfield_5.anchor.x = Math.round(this.pfield_5.width * 0.5) / this.pfield_5.width;

this.pfield_6 = this.game.add.text(this.game.world.centerX, 730, AdSpend.resources.ico6text, styleFooter);
this.pfield_6.anchor.set(0.5);
this.pfield_6.anchor.x = Math.round(this.pfield_6.width * 0.5) / this.pfield_6.width;

		//
	//
//

	this.tweenArrow = this.game.add.tween(this.arrowd).to( { y: -150 }, 4000, Phaser.Easing.Exponential.Out, true, 1000);

	this.tweenText = this.game.add.tween(this.pfield_4).to( { alpha: 1 }, 4000, Phaser.Easing.Exponential.Out, true, 6000);
	this.tweenText2 = this.game.add.tween(this.pfield_4l).to( { alpha: 1 }, 4000, Phaser.Easing.Exponential.Out, true, 6000);

	this.arrowl.anchor.set(1);
	this.arrowl.scale.x = 0;
	this.arrowb.scale.x = 0;

	this.tweenCracks = this.game.add.tween(this.cracks).to( { alpha: 1 }, 3000, Phaser.Easing.Linear.None, true, 2000);

	this.tweenArrowS = this.game.add.tween(this.arrowl.scale).to( { x: 1 }, 3000, Phaser.Easing.Linear.None, true, 3000);

	this.tweenArrowB = this.game.add.tween(this.arrowb.scale).to( { x: 1 }, 3000, Phaser.Easing.Linear.None, true, 3000);

	this.tweenArrowLoop = this.game.add.tween(this.coinb).to( { y: 300 }, 3000, Phaser.Easing.Linear.None, true, 0, 5000);

	this.tweenArrowLoop2 = this.game.add.tween(this.coind).to( { y: 260 }, 3000, Phaser.Easing.Linear.None, true, 1000, 5000);

	},

/////////////////////////////////////////////////

	update: function()
	{

	},

	render: function()
	{

	}

}