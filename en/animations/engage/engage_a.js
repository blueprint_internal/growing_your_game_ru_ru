
function EngageA(resources)
{
	EngageA.resources = resources;
}
EngageA.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(800, 450, Phaser.CANVAS, 'EngageA', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this });
	},
	preload: function()
	{
		this.game.scale.maxWidth = 800;
		this.game.scale.maxHeight = 450;
		this.game.stage.backgroundColor = '#ffffff';
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('bckg', EngageA.resources.bckg);
		this.game.load.image('line1', EngageA.resources.line1);
		this.game.load.image('line2', EngageA.resources.line2);
		this.game.load.image('line3', EngageA.resources.line3);
		this.game.input.touch.preventDefault = false;

	},
	create: function(evt)
	{

    this.parent.buildAnimation();

	},
	buildAnimation: function()
	{
		this.tweenCount = 0;

        //Background
		this.bckg = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'bckg');
		this.bckg.anchor.set(0.5);

		var styleFunnel2 = {font:"bold 24px freight-sans-pro", fill: "#FFFFFF", wordWrap: true, wordWrapWidth: 500, align: "Center",lineSpacing: -10 };

		//ARRAYS
		this.lineArray = new Array();
		this.localArray = EngageA.resources.textArray

		//LINES
		this.line1 = this.game.add.sprite(90, 145, 'line1');
		this.line1.alpha = 0;
		this.lineArray.push(this.line1)

		this.line2 = this.game.add.sprite(164, 142, 'line2');
		this.line2.alpha = 0;
		this.lineArray.push(this.line2)

		this.line3 = this.game.add.sprite(164, 142, 'line3');
		this.line3.alpha = 0;
		this.lineArray.push(this.line3)

		//FIELD
		this.pfield_1 = this.game.add.text(250, 35, this.localArray[0], styleFunnel2);
		this.pfield_1.setText(this.localArray[0]);

		//TWEEN

		var tween1 = this.game.add.tween(this.line1).to({alpha: 1}, 2500, Phaser.Easing.Cubic.Out, true).onComplete.add(function () {
			this.nextText();
		}, this);

	},
		nextText: function(evt)
	{

		if (this.tweenCount > 2  ){
		this.tweenCount = 0;
		}

		this.pfield_1.setText(this.localArray[this.tweenCount]);

		this.lineArray[0].alpha = 0;
		this.lineArray[1].alpha = 0;
		this.lineArray[2].alpha = 0;

		this.lineArray[this.tweenCount].alpha = 1;

		
		var tweenLine1 = this.game.add.tween(this.lineArray[this.tweenCount]).to({alpha: 1}, 2500, Phaser.Easing.Cubic.Out, true).onComplete.add(function () {
				this.nextText();
		}, this);

		this.tweenCount ++
	}

}