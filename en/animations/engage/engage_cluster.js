
function EngageCluster(resources)
{
	EngageCluster.resources = resources;
}
EngageCluster.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(800, 450, Phaser.CANVAS, 'EngageCluster', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this });
	},
	preload: function()
	{
		this.game.scale.maxWidth = 800;
		this.game.scale.maxHeight = 450;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('bckg', EngageCluster.resources.bckg);
		this.game.load.image('line1', EngageCluster.resources.line1);
		this.game.load.image('line2', EngageCluster.resources.line2);
		this.game.load.image('line3', EngageCluster.resources.line3);
		this.game.input.touch.preventDefault = false;

	},
	create: function(evt)
	{

    this.parent.buildAnimation();

	},
	buildAnimation: function()
	{
		this.tweenCount = 0;

		//this.lineArray = new Array();
		this.localArray = EngageCluster.resources.textArray;
		this.localArray2 = EngageCluster.resources.textArray2;

        //Background
		this.bckg = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'bckg');
		this.bckg.anchor.set(0.5);

		var styleFunnel2 = {font:"bold 18px freight-sans-pro", fill: "#FFFFFF", wordWrap: true, wordWrapWidth: 200, align: "Center",lineSpacing: -10 };

		var styleFunnel4 = {font:"bold 32px freight-sans-pro", fill: "#FFFFFF", wordWrap: true, wordWrapWidth: 800, align: "Center",lineSpacing: -10 };


		this.pfield_title = this.game.add.text(this.game.world.centerX, 40, EngageCluster.resources.ico1text, styleFunnel4);
		this.pfield_title.anchor.set(.5);

   		//FIELD
		this.pfield_1 = this.game.add.text(80, 260, this.localArray[0], styleFunnel2);

		this.pfield_2 = this.game.add.text(300, 260, this.localArray[1], styleFunnel2);

		this.pfield_3 = this.game.add.text(540, 260, this.localArray[2], styleFunnel2);

		this.pfield_4 = this.game.add.text(80, 220, this.localArray2[0], styleFunnel2);

		this.pfield_5 = this.game.add.text(300, 220, this.localArray2[1], styleFunnel2);

		this.pfield_6 = this.game.add.text(540, 220, this.localArray2[2], styleFunnel2);


	}

}