
function Casestudy(resources)
{
	Casestudy.resources = resources;
}
Casestudy.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(800, 450, Phaser.CANVAS, 'Casestudy', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this });
	},
	preload: function()
	{
		this.game.scale.maxWidth = 800;
		this.game.scale.maxHeight = 450;
		this.game.stage.backgroundColor = '#ffffff';
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('bckg', Casestudy.resources.bckg);
		this.game.load.image('line1', Casestudy.resources.line1);
		this.game.load.image('line2', Casestudy.resources.line2);
		this.game.load.image('line3', Casestudy.resources.line3);
		this.game.input.touch.preventDefault = false;

	},
	create: function(evt)
	{

    this.parent.buildAnimation();

	},
	buildAnimation: function()
	{
		this.tweenCount = 0;

        //Background
		this.bckg = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'bckg');
		this.bckg.anchor.set(0.5);

		var styleFunnel2 = {font:"bold 26px freight-sans-pro", fill: "#FFFFFF", wordWrap: true, wordWrapWidth: 470, align: "Center"};


		var styleFunnel3 = {font:"bold 30px freight-sans-pro", fill: "#FFFFFF", wordWrap: true, wordWrapWidth: 520, align: "Center",lineSpacing: -10 };

		var styleFunnel4 = {font:"22px freight-sans-pro", fill: "#FFFFFF", wordWrap: true, wordWrapWidth: 520, align: "Center",lineSpacing: -10 };

		var styleFunnel5 = {font:"18px freight-sans-pro", fill: "#FFFFFF", wordWrap: true, wordWrapWidth: 520, align: "Center",lineSpacing: -10 };

		this.pfield_1 = this.game.add.text(25, 25, Casestudy.resources.ico1text, styleFunnel3);

		this.pfield_2 = this.game.add.text(25, 25, Casestudy.resources.ico2text, styleFunnel4);

		this.pfield_3 = this.game.add.text(25, 200, Casestudy.resources.ico3text, styleFunnel5);

		//ARRAYS
		this.lineArray = new Array();
		this.localArray = Casestudy.resources.textArray

		//LINES
		this.line1 = this.game.add.sprite(-900, 145, 'line1');
		this.line1.alpha = 0;
		this.lineArray.push(this.line1)

		this.line2 = this.game.add.sprite(-1640, 142, 'line2');
		this.line2.alpha = 0;
		this.lineArray.push(this.line2)

		this.line3 = this.game.add.sprite(-1640, 142, 'line3');
		this.line3.alpha = 0;
		this.lineArray.push(this.line3)

		//FIELD
		this.pfield_9 = this.game.add.text(50, 290, this.localArray[0], styleFunnel2);
		this.pfield_9.setText(this.localArray[0]);
		this.pfield_9.lineSpacing = -10;

		//TWEEN

		var tween1 = this.game.add.tween(this.line1).to({alpha: 1}, 2500, Phaser.Easing.Cubic.Out, true).onComplete.add(function () {
			this.nextText();
		}, this);

	},
		nextText: function(evt)
	{

		if (this.tweenCount > 2  ){
		this.tweenCount = 0;
		}

		this.pfield_9.setText(this.localArray[this.tweenCount]);

		this.lineArray[0].alpha = 0;
		this.lineArray[1].alpha = 0;
		this.lineArray[2].alpha = 0;

		this.lineArray[this.tweenCount].alpha = 1;

		
		var tweenLine1 = this.game.add.tween(this.lineArray[this.tweenCount]).to({alpha: 1}, 5000, Phaser.Easing.Cubic.Out, true).onComplete.add(function () {
				this.nextText();
		}, this);

		this.tweenCount ++
	},

/////////////////////////////////////////////////

	update: function()
	{
	},

	render: function()
	{
	}

}