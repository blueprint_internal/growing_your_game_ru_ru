
function Cycle(resources)
{
	Cycle.resources = resources;
}
Cycle.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(800, 800, Phaser.CANVAS, 'Cycle', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this });
	},

	preload: function()
	{
    this.game.scale.maxWidth = 800;
    this.game.scale.maxHeight = 800;
    this.game.stage.backgroundColor = '#ffffff';
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('bckg', Cycle.resources.bckg);
    this.game.load.image('icon1', Cycle.resources.icon1);
    this.game.load.image('icon2', Cycle.resources.icon2);
    this.game.load.image('icon3', Cycle.resources.icon3);
    this.game.load.image('icon4', Cycle.resources.icon4);
    this.game.load.image('icon5', Cycle.resources.icon5);
    this.game.load.image('arrowrotor', Cycle.resources.arrowrotor);
    this.game.load.image('redcirc', Cycle.resources.redcircle);
    //
        //should prevent mobile scroll issue
    this.game.input.touch.preventDefault = false;
	},

	create: function(evt)
	{
    //Call the animation build function
    this.parent.buildAnimation();
	},


	buildAnimation: function()
	{

    this.hilite = Cycle.resources.hilite 
    //Background
    this.bckg = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'bckg');
    this.bckg.anchor.set(0.5);

  this.arrowrotor = this.game.add.sprite(this.game.world.centerX ,this.game.world.centerY, 'arrowrotor');//
    this.arrowrotor.anchor.set(0, 1);
   var hexColor = "#4963a3"
   var hexColor1 = "#888888"
   var hexColor2 = "#888888"
   var hexColor3 = "#888888"
   var hexColor4 = "#888888"

    //this.hilite
    switch(this.hilite){
    case "0" :
    this.redcircle = this.game.add.sprite(this.game.world.centerX - 2000,2000, 'redcirc');//
    this.redcircle.anchor.set(0.5);

   hexColor1 = "#FFFFFF"
   hexColor2 = "#4963a3"
   hexColor3 = "#4963a3"
   hexColor4 = "#FFFFFF"

    break;
    	case "1" :
    this.redcircle = this.game.add.sprite(this.game.world.centerX - 200,200, 'redcirc');//
    this.redcircle.anchor.set(0.5);
    hexColor1 = "#4963a3"
    break;
    	case "2":
    this.redcircle = this.game.add.sprite(this.game.world.centerX + 200,200, 'redcirc');//
    this.redcircle.anchor.set(0.5);
    hexColor2 = "#4963a3"
    break;
    	case "3":
    this.redcircle = this.game.add.sprite(this.game.world.centerX - 200,600, 'redcirc');//
    this.redcircle.anchor.set(0.5);
    hexColor3 = "#4963a3"
    break;
    	case "4":
    this.redcircle = this.game.add.sprite(this.game.world.centerX + 200,600, 'redcirc');//
    this.redcircle.anchor.set(0.5);
    hexColor4 = "#4963a3"
    break;
     default:
      console.log("default")
    }




    this.icon1 = this.game.add.sprite(this.game.world.centerX - 180,220, 'icon1');//
    this.icon1.anchor.set(0.5);
    this.icon2 = this.game.add.sprite(this.game.world.centerX + 180,220, 'icon2');//
    this.icon2.anchor.set(0.5);

    this.icon3 = this.game.add.sprite(this.game.world.centerX - 180,580, 'icon3');//
    this.icon3.anchor.set(0.5);
    this.icon4 = this.game.add.sprite(this.game.world.centerX + 180,580, 'icon4');//
    this.icon4.anchor.set(0.5);

    this.icon5 = this.game.add.sprite(this.game.world.centerX ,this.game.world.centerY, 'icon5');//
    this.icon5.anchor.set(0.5);



    var redHighlite = {font:"bold 34px vanilla", fill: hexColor, align: "Center",lineSpacing: -10 };

    var oneHighlite = {font:"bold 34px vanilla", fill: hexColor1, align: "right",lineSpacing: -10 };


    var twoHighlite = {font:"bold 34px vanilla", fill: hexColor2, align: "left",lineSpacing: -10 };

    var threeHighlite = {font:"bold 34px vanilla", fill: hexColor3, align: "right",lineSpacing: -10 };

    var fourHighlite = {font:"bold 34px vanilla", fill: hexColor4, align: "left",lineSpacing: -10 };

   oneHighlite.align='left';
   twoHighlite.align='left'; 
   threeHighlite.align='left';
   fourHighlite.align='left';


   this.icofield_1 = this.game.add.text((this.game.world.centerX / 2), 80, Cycle.resources.ico1text, oneHighlite);
   //this.icofield_1.anchor.set(1,0);

   this.icofield_2 = this.game.add.text(this.game.world.centerX +((this.game.world.centerX / 2)), 80, Cycle.resources.ico2text, twoHighlite);
  // this.icofield_2.anchor.set(-1,0);

   this.icofield_3 = this.game.add.text((this.game.world.centerX / 2), 710, Cycle.resources.ico3text, threeHighlite);
   //this.icofield_3.anchor.set(1,0);

   this.icofield_4 = this.game.add.text(this.game.world.centerX +((this.game.world.centerX / 2)), 710, Cycle.resources.ico4text, fourHighlite);
  // this.icofield_4.anchor.set(-1,0);

   this.icofield_5 = this.game.add.text(this.game.world.centerX, this.game.world.centerY + 100, Cycle.resources.ico5text, redHighlite);
   

  this.icofield_1.anchor.set(0.5);
  this.icofield_2.anchor.set(0.5);
  this.icofield_3.anchor.set(0.5);
  this.icofield_4.anchor.set(0.5);
  this.icofield_5.anchor.set(0.5);

	}
  ,
     update: function()
  {
    this.parent.arrowrotor.rotation += .01;
    //console.log("rotor" + this.parent.arrowrotor.rotation);
   //this.arrowrotor.x += 1;

  }

}